import os
from flask_restful import reqparse, Resource, fields
from flask import send_file, current_app
import redis
from rq import Queue, Connection
from web.scraper import scrape_img, scrape_text 
import sys
import requests
import zipfile
from urllib.parse import urlparse


def map_url2path(url):
    parse = urlparse(url)
    dir_name = parse.netloc + parse.path
    return f"data/{dir_name}"

def zip_dir(path):
    zipf = zipfile.ZipFile(os.path.split(path).head, 'w')
    for root, dirs, files in os.walk(path):
        for filename in files:
            zipf.write(os.path.join(root, filename), filename)
    zpif.close()


def type_check(download_type):
    if download_type in ['img', 'txt']:
        return download_type
    else:
        raise ValueError("Wrong type")

def url_check(url):
    try:
        response = requests.head(url)
        if response.status_code == 200:
            return url
    except:
        raise ValueError("Wrong url") 

parser = reqparse.RequestParser()
parser.add_argument('url', type=url_check, required=True)
parser.add_argument('type', type=type_check, required=True)

class CreateTask(Resource):
    def post(self):
        #Parse arguments from POST
        args = parser.parse_args()
        url = args['url']
        order_type = args['type']

        with Connection(redis.from_url(current_app.config["REDIS_URL"])):
            q = Queue()

            if order_type == 'img':
                task = q.enqueue(scrape_img, args['url'])
            if order_type == 'txt':
                task = q.enqueue(scrape_text, args['url'])

            response_object = {
                    "status": "success",
                    "data": {
                        "task_id": task.get_id()
                    }
            }
            return response_object, 202

class StatusTask(Resource):
    def get(self, task_id):

        with Connection(redis.from_url(current_app.config["REDIS_URL"])):
            q = Queue()
            task = q.fetch_job(task_id)

        if task:
            response_object = {
                    "status": "success", 
                    "data": {
                        "task_id": task.get_id(),
                        "task_status": task.get_status()
                        },
                    }
        else:
            response_object = {"status": "Wrong id"}

        return response_object

class DownloadTask(Resource):
    def post(self):
        args = parser.parse_args()
        url = args['url']
        order_type = args['type']

        path = map_url2path(url)

        if order_type == 'img':
            path = os.path.join(path, 'img.zip')
        if order_type == 'txt':
            path = os.path.join(path, 'page_content.txt')

        #Check if file is in the system
        if os.path.exists(path):
            return send_file(os.path.join("..", path))
        else:
            response_object = {"status": "There is no file in system"}
            return response_object
