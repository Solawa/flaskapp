import os

from flask import Flask
from flask_restful import Api
from web.controler import *

def create_app():
    app = Flask(__name__)
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    api = Api(app)

    api.add_resource(CreateTask,'/task')
    api.add_resource(StatusTask,'/task/<task_id>')
    api.add_resource(DownloadTask,'/save')

    return app
