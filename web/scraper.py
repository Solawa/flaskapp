from bs4 import BeautifulSoup
import os
import zipfile
import requests
import time
from urllib.parse import urlparse

tags = ['src', 'data-src']
format_support = ['jpg', 'png', 'jpeg', 'bmp', 'gif'] 

def map_url2path(url):
    parse = urlparse(url)
    dir_name = parse.netloc + parse.path
    return os.path.join('data', dir_name)

def getUrl(url):
    uri = urlparse(url)
    return f"{uri.scheme}://{uri.netloc}/"

def norm_url(url, host):
    parser = urlparse(url)
    
    if parser.scheme == '':
        parser = parser._replace(scheme='http')
    if parser.netloc == '':
        parser = parser._replace(netloc=host)
    print(parser.getUrl())
    return parser.geturl()


def zip_imgs(path, ):
    zipf = zipfile.ZipFile(os.path.join(path, 'img.zip'), 'w')
    for root, dirs, files in os.walk(path):
        for filename in files:
            if os.path.splitext(filename)[1][1:] in format_support:
                zipf.write(os.path.join(path, "img", filename), filename)
    zipf.close()

def get_filename(url):
    filename = url.split('/')[-1] 
    if '?' in filename:
        filename = filename.split('?')[0]
    return filename

def create_dir(url, data_type):
    parse = urlparse(url)
    dir_name = parse.netloc + parse.path
    path = os.path.join("data", dir_name, data_type)
    if not os.path.exists(path):
        os.makedirs(path)

def get_hostname(url):
    parse = urlparse(url)
    return parse.netloc

def filename2ext(filename):
    return filename.split('.')[1]

def scrape_img(url):

    create_dir(url, 'img')
    path = map_url2path(url)
    hostname = get_hostname(url)

    #retriving url of image and iterating
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    
    for html_element in soup.findAll('img'):

        #retrieve url from element
        for tag in tags:
            img_url = html_element.get(tag)
            if img_url is not None:
                break
        #theres no url in element
        else:
            continue

        try:
            #Check if file format supported
            filename = get_filename(img_url)
            if filename2ext(filename) not in format_support:
                continue

            #Check if url is accessible
            img_url = norm_url(img_url, hostname) 


            response = requests.get(img_url, stream = True)
            if response.status_code == 200:
                with open(os.path.join(path, 'img', filename), 'wb') as f:
                    for chunk in response:
                        f.write(chunk)

        except IndexError:
            pass

    #Zipping dir, prepare for download
    zip_imgs(path)

def scrape_text(url):
   
    create_dir(url, 'txt')

    body = requests.get(url)
    soup = BeautifulSoup(body.text, "html.parser")

    #Delete redundant tags
    delete_tags = ['style', 'script', '[document]', 'head', 'title']
    [s.extract() for s in soup(delete_tags)]
    text = soup.getText()

    #Clean with multiple \n
    path = map_url2path(url)
    with open(os.path.join(path, "page_content.txt"), 'w', encoding="utf-8") as f:
        f.write(text)
