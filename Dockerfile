FROM python:3.8.2-buster

EXPOSE 5000

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY . /app
WORKDIR /app
