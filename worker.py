from flask import current_app
from rq import Worker, Queue, Connection
import redis

conn = redis.from_url("redis://redis:6379/0")
with Connection(conn):
    worker = Worker(["default"])
    worker.work()
