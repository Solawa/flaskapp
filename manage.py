from web import create_app
from flask.cli import FlaskGroup
import unittest

app = create_app()
cli = FlaskGroup(create_app=create_app)

@cli.command("test")
def test():
    pass

if __name__ == '__main__':
    cli()
