# FlaskApp

## Technologies
*  Python
*  Reddis
*  Flask
*  Docker

## General info
The application enable user to send orders to download content of a web page.
In order to be able handle many requests(downloading can takes some time), orders are send via Reddis to another system, which is responsible for downloading and saving content.


## Run app
To build the full project, use 
```
docker-compose up --build
```
at location of docker-compose.yml file

## Endpoints:
1. /task POST - order a request to download a web content -args [url: url address, type: type of data to download (img or txt)]
2. /task/<task_id>; GET - return task status
3. /save POST - download a web content from server -args [url: url address, type: type of data to download (img or txt)]

